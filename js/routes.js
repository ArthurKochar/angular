angular.module("blogApp").config(function($stateProvider, $urlRouterProvider) {
 
	$stateProvider
	.state('login', {
	    url: "/login",
	    templateUrl: "views/login.html",
	    controller: "loginController"
	      
    })
    .state('register', {
	    url: "/register",
	    templateUrl: "views/register.html",
	    controller: "registerController"
	      
    })
    .state('home', {
	    url: "/home",
	    templateUrl: "views/home.html",
	    controller: "homeController"
	      
    })
    .state('category', {
	    url: "/category/:id",
	    templateUrl: "views/category.html",
	    controller: "homeController"
	})
	.state('posts', {
	    url: "/posts/:id",
	    templateUrl: "views/posts.html",
	    controller: "postController"
	})
	.state('postEdit',{
		url: "/postEdit/:id",
		templateUrl:"views/postEdit.html",
		controller: "postController"
	})
	.state('addPost',{
		url: "/addPost",
		templateUrl:"views/postAdd.html",
		controller: "postController"
	})
	

  	$urlRouterProvider.otherwise("/");
});