angular.module('blogApp').controller('loginController',['$scope', '$http', '$stateParams','$location', '$rootScope','$state', function($scope, $http, $stateParams, $location, $rootScope,$state) {
    $scope.inputs = {};
    $rootScope.user = '';
    $rootScope.loggedIn = false;
    $rootScope.data = {};
    

    $scope.submit = function(inputs){
        inputs.api = true;
        $scope.inputs = inputs;
        $http.post('/api/login',$scope.inputs).then(function(response){
            if(response.status == 200 && response.data.login){
                localStorage.setItem('id',response.data.id)
                localStorage.setItem('user',response.data.username);
                localStorage.setItem('data',JSON.stringify(response.data));
                $rootScope.user = localStorage['user'];
                $rootScope.id = localStorage['id'];
                localStorage.setItem('loggedIn',true);
                $rootScope.loggedIn = localStorage['loggedIn'];
               
                $state.go('home');

            }

           
        },
        function(response){
            $rootScope.messages = response.data;
        });
        
        
    }
    $rootScope.user = localStorage['user'];
    $rootScope.id = localStorage['id'];
    $rootScope.loggedIn = localStorage['loggedIn'];
    $rootScope.data = localStorage['data'];
    $scope.logout = function() {
        localStorage.clear();
        $rootScope.loggedIn = false;
        $state.go('login');
        
    };


}]);
