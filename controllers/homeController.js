angular.module('blogApp').controller('homeController',['$scope', '$http', '$stateParams','$location', '$rootScope','$state', function($scope, $http, $stateParams, $location, $rootScope,$state) {
  if(!localStorage['loggedIn']){
    $state.go('login');
    return false;
  }
  $scope.data = JSON.parse(localStorage['data']);
  if($state.current.name == 'home')
    {
      $http.get('/api/categories').then(function(response){
  		  $rootScope.categ = response.data.category;
        localStorage.setItem('category',JSON.stringify(response.data.category))    	
			})  
    }else if($state.current.name == 'category')
    {  	
      $http.get('/api/getCategoryPosts/' + $state.params.id).then(function(response){
      $rootScope.posts = response.data.posts;
    	$scope.selected =response.data.selected;
    	$scope.categ = JSON.parse(localStorage['category']);
    })

    }	
    $scope.addCat = function(inputs){
    	$http({
		    method: 'POST',
		    url: '/api/categories',
		    data: inputs,
		    headers: {
		        'Content-Type': 'application/json'
		    }}).then(function(result) {
		    	if(result.status == 201){
		    		$rootScope.categ.push(inputs)
            localStorage.setItem('category',JSON.stringify($rootScope.categ))
		    	}
		           
		       },
           function(result){
                  $scope.errors='';
                  $scope.errors = result.data;
                  console.log(result.data);
              }
             );
}
    	
  



}]);
