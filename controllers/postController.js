angular.module('blogApp').controller('postController',['$scope', '$http', '$state','$location', '$rootScope','Upload', function($scope, $http, $state, $location, $rootScope, Upload) {
 	$rootScope.categ = JSON.parse(localStorage['category']);
 	if($state.current.name == 'posts')
    {  	

        $http.get('/api/posts/' + $state.params.id).then(function(response){

    		$rootScope.post = response.data.post;

            
    	})

    }else if ($state.current.name == "postEdit")
    {
    	$rootScope.categ = JSON.parse(localStorage['category']);
    	$http.get('/api/posts/' + $state.params.id).then(function(response){
			$rootScope.post = response.data.post;
			if($rootScope.post){
				$rootScope.categ = JSON.parse(localStorage['category']);
			}
		})
    	$scope.uploadPic = function(file) 
        {
        
            if(file)
            {
            	if(!file.name && !file.size){
            		file = '';
            	}
                
                file.upload = Upload.upload({
                    url: '/api/posts/' +$state.params.id,
                    data: {_method: 'PUT',title: $scope.post.title,category:$scope.post.categories_id,description:$scope.post.description, avatar: file},
                });

                file.upload.then(function (response) {
                    $state.go('home');
                });

            }
            
        }
    } else if($state.current.name == 'addPost')
    {
        
        $scope.uploadPic = function(file)
        {
        
            if(file)
            {
            	
                file.upload = Upload.upload({
                    url: '/api/posts',
                    data: {title: $scope.title,category:$scope.categories_id,description:$scope.description, avatar: file},
                });

                file.upload.then(function (response) 
                {
                    $state.go('home');
                });

            }
            
        }
       
    }
    $scope.delete = function(){
    
        var id = $scope.post.id;
        $http.delete('/api/posts/'+id).then(function(response){
            $state.go('home');
        }) 

    }	


 }]);