angular.module('blogApp').controller('registerController',['$http', '$scope','$rootScope', 'Upload','$state', function($http, $scope,$rootScope,Upload, $state) {
    $scope.inputs = {};
    
    $scope.submit = function(file)
        {
        
            if(file)
            {
                file.upload = Upload.upload({
                    url: '/api/register',
                    data: {
                        username: $scope.username,
                        lastname: $scope.lastname,
                        firstname: $scope.firstname,
                        avatar: file,
                        email: $scope.email,
                        phone_number: $scope.phone_number,
                        password: $scope.password,
                        password_confirmation: $scope.password_confirmation
                     },
                });

                file.upload.then(function (response) 
                {
                   
                    if(response.status == 200 && !response.data.errors){
                       $state.go('login');
                    }
                    
                },function(result){
                            $scope.errors='';
                            $scope.errors = result.data;
                        });

            }else{
                $http({
                    method: 'POST',
                    url: '/api/register',
                    data: {
                        username: $scope.username,
                        lastname: $scope.lastname,
                        firstname: $scope.firstname,
                        email: $scope.email,
                        phone_number: $scope.phone_number,
                        password: $scope.password,
                        password_confirmation: $scope.password_confirmation
                     },
                    headers: {
                        'Content-Type': 'application/json'
                    }}).then(function(result) {
                        if(response.status == 200 && !response.data.errors){
                            $state.go('login');
                        }
                           
                       },
                       function(result){
                            $scope.errors='';
                            $scope.errors = result.data;
                        }
                       );
            }
            
        }
}]);